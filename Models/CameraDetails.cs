﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class CameraDetails
    {
        public CameraDetails() {
            this.Secondary = "N/A";
            this.Primary = "N/A";
            this.Features = "N/A";
            this.Video = "N/A";
        }
        string _secondary;
        string _video;
        string _features;
        string _primary;

        public string Primary
        {
            get { return _primary; }
            set { _primary = value; }
        }

        public string Features
        {
            get { return _features; }
            set { _features = value; }
        }

        public string Video
        {
            get { return _video; }
            set { _video = value; }
        }



        public string Secondary
        {
            get { return _secondary; }
            set { _secondary = value; }
        }
    }
}