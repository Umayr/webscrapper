﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class GeneralDetails
    {
        public GeneralDetails()
        {
            this.Network2G = "N/A";
            this.Network3G = "N/A";
            this.Sim = "N/A";
            this.Status = "N/A";
        }

        string _network2G;
        string _network3G;
        string _sim;
        string _status;



        public string Network2G
        {
            get { return _network2G; }
            set { _network2G = value; }
        }


        public string Network3G
        {
            get { return _network3G; }
            set { _network3G = value; }
        }


        public string Sim
        {
            get { return _sim; }
            set { _sim = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

    }
}