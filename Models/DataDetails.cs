﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class DataDetails
    {
        string _USB;
        string _bluetooth;
        string _WAN;
        string _speed;
        string _EDGE;
        string _GPRS;

        public string GPRS
        {
            get { return _GPRS; }
            set { _GPRS = value; }
        }

        public string EDGE
        {
            get { return _EDGE; }
            set { _EDGE = value; }
        }

        public string Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public string WAN
        {
            get { return _WAN; }
            set { _WAN = value; }
        }

        public string Bluetooth
        {
            get { return _bluetooth; }
            set { _bluetooth = value; }
        }

        public string USB
        {
            get { return _USB; }
            set { _USB = value; }
        }
        public DataDetails()
        {
            this.USB = "N/A";
            this.WAN = "N/A";
            this.Speed = "N/A";
            this.GPRS = "N/A";
            this.EDGE = "N/A";
            this.Bluetooth = "N/A";
        }
    }
}