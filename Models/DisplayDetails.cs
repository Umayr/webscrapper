﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class DisplayDetails
    {
        public DisplayDetails() {
            this.Protection = "N/A";
            this.Multitouch = "N/A";
            this.Size = "N/A";
            this.Type = "N/A";
        }
        string _protection;
        string _multitouch;
        string _size;
        string _type;

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string Size
        {
            get { return _size; }
            set { _size = value; }
        }

        public string Multitouch
        {
            get { return _multitouch; }
            set { _multitouch = value; }
        }

        public string Protection
        {
            get { return _protection; }
            set { _protection = value; }
        }
    }
}