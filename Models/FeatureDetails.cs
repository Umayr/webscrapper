﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class FeatureDetails
    {
        public FeatureDetails() {
            this.Colors = "N/A";
            this.Browser = "N/A";
            this.CPU = "N/A";
            this.GPS = "N/A";
            this.GPU = "N/A";
            this.Java = "N/A";
            this.Messaging = "N/A";
            this.OS = "N/A";
            this.Radio = "N/A";
            this.Sensors = "N/A";
            
        }
        string _colors;
        string _java;
        string _GPS;
        string _radio;
        string _browser;
        string _messaging;
        string _sensors;
        string _GPU;
        string _CPU;
        string _OS;

        public string OS
        {
            get { return _OS; }
            set { _OS = value; }
        }

        public string CPU
        {
            get { return _CPU; }            set { _CPU = value; }
        }

        public string GPU
        {
            get { return _GPU; }
            set { _GPU = value; }
        }

        public string Sensors
        {
            get { return _sensors; }
            set { _sensors = value; }
        }

        public string Messaging
        {
            get { return _messaging; }
            set { _messaging = value; }
        }

        public string Browser
        {
            get { return _browser; }
            set { _browser = value; }
        }

        public string Radio
        {
            get { return _radio; }
            set { _radio = value; }
        }

        public string GPS
        {
            get { return _GPS; }
            set { _GPS = value; }
        }

        public string Java
        {
            get { return _java; }
            set { _java = value; }
        }

        public string Colors
        {
            get { return _colors; }
            set { _colors = value; }
        }

    }
}