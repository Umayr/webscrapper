﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace MobileAPIv2.Models
{
    public class Process
    {
        public static DataTable GetMobileFromDatabase()
        {

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LocalDatabase"].ToString());
            string Q = @"SELECT * FROM vw_displayAllMobiles";
            SqlCommand cmd = new SqlCommand(Q, con);
            cmd.CommandTimeout = 9999;
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            con.Close();
            DataTable dt = new DataTable();
            da.Fill(dt);
            da.Dispose();
            return dt;

        }

        public static string GetResponse(string URL)
        {
            WebClient oWC = new WebClient();
            StringBuilder AbsoluteURL = new StringBuilder("http://www.gsmarena.com/");
            AbsoluteURL.Append(URL);
            return oWC.DownloadString(AbsoluteURL.ToString());
        }
    }
}