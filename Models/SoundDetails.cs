﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class SoundDetails
    {
        string _loudspeaker;
        string _alertTypes;

        public SoundDetails()
        {
            Loudspeaker = "N/A";
            AlertTypes = "N/A";
        }
        public string AlertTypes
        {
            get { return _alertTypes; }
            set { _alertTypes = value; }
        }

        public string Loudspeaker
        {
            get { return _loudspeaker; }
            set { _loudspeaker = value; }
        }
    }
}