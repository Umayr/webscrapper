﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class RatingInfo
    {
        string _performance;

        public string Performance
        {
            get { return _performance; }
            set { _performance = value; }
        }
        string _features;

        public string Features
        {
            get { return _features; }
            set { _features = value; }
        }
        string _design;

        public string Design
        {
            get { return _design; }
            set { _design = value; }
        }

    }
}