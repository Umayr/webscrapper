﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
namespace MobileApi.Models
{
    public class CustomResult : ActionResult
    {
        Result _R = null;

        public Result R
        {
            get { return _R; }
            set { _R = value; }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            HttpContextBase C = context.HttpContext;
            C.Response.Buffer = true;
            C.Response.ContentType = "application/json";

            C.Response.Write(JsonConvert.SerializeObject(R, Formatting.Indented));
        }
    }
}