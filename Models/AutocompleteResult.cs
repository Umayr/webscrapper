﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MobileAPIv3.Models
{
    public class AutocompleteResult:ActionResult
    {
        List<string> _R = null;

        public List<string> R
        {
            get { return _R; }
            set { _R = value; }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            HttpContextBase C = context.HttpContext;
            C.Response.Buffer = true;
            C.Response.ContentType = "application/json";

            C.Response.Write(JsonConvert.SerializeObject(R, Formatting.None));
        }
    }
}