﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace MobileApi.Models
{
    public class Mobile
    {
        private static string RAWURL = "http://www.gsmarena.com/";
        private string IMAGE_URL_INIT = "http://st2.gsmarena.com/vv/bigpic/";
        string _id;
        string _name;
        string _imageURL;
        string _manufacturer;
        BatteryDetails _battery;
        BodyDetails _body;
        CameraDetails _camera;
        DataDetails _data;
        DisplayDetails _display;
        FeatureDetails _feature;
        GeneralDetails _general;
        MemoryDetails _memory;
        RatingInfo _userRating;
        SoundDetails _sound;
        private bool _status;

        public string Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string ImageURL
        {
            get { return _imageURL; }
            set { _imageURL = value; }
        }
        public BodyDetails Body
        {
            get { return _body; }
            set { _body = value; }
        }
        public BatteryDetails Battery
        {
            get { return _battery; }
            set { _battery = value; }
        }
        public CameraDetails Camera
        {
            get { return _camera; }
            set { _camera = value; }
        }
        public DisplayDetails Display
        {
            get { return _display; }
            set { _display = value; }
        }
        public DataDetails Data
        {
            get { return _data; }
            set { _data = value; }
        }
        public FeatureDetails Feature
        {
            get { return _feature; }
            set { _feature = value; }
        }
        public GeneralDetails General
        {
            get { return _general; }
            set { _general = value; }
        }
        public MemoryDetails Memory
        {
            get { return _memory; }
            set { _memory = value; }
        }
        public SoundDetails Sound
        {
            get { return _sound; }
            set { _sound = value; }
        }
        public RatingInfo UserRating
        {
            get { return _userRating; }
            set { _userRating = value; }
        }
        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }
        private void instantiateAllObject()
        {
            this.Battery = new BatteryDetails();
            this.Body = new BodyDetails();
            this.Camera = new CameraDetails();
            this.Data = new DataDetails();
            this.Display = new DisplayDetails();
            this.Feature = new FeatureDetails();
            this.General = new GeneralDetails();
            this.Memory = new MemoryDetails();
            this.Sound = new SoundDetails();
            this.UserRating = new RatingInfo();
        }

        public Mobile(string Name, string URL)
        {

            HtmlDocument doc = new HtmlDocument();
            this.Status = false;
            try
            {
                Id = Guid.NewGuid().ToString().Replace("-", string.Empty);
                doc.Load(Mobile.getResponseStream(RAWURL + URL));
                HtmlNode DocNode = doc.DocumentNode;
                this.Manufacturer = URL.Split('_')[0];

                try { this.Name = DocNode.SelectSingleNode("//*[@id=\"ttl\"]/h1").InnerText; }
                catch (NullReferenceException)
                {
                    this.Name = Name;
                }

                try { this.ImageURL = DocNode.SelectSingleNode("//*[@id=\"specs-cp-pic\"]/a/img").Attributes["src"].Value; }
                catch (NullReferenceException)
                {
                    this.ImageURL = "N/A";
                }
                HtmlNodeCollection SpecInfo = DocNode.SelectNodes("//*[@id=\"specs-list\"]/table");

                string _specName = null;
                this.UserRating = fetchRatingInfo(DocNode);
                foreach (HtmlNode N in SpecInfo)
                {

                    _specName = N.Element("tr").Element("th").InnerText.ToLower();
                    switch (_specName)
                    {

                        case "general":
                            this.General = fetchGeneralDetails(N);
                            break;
                        case "body":
                            this.Body = fetchBodyDetails(N);
                            break;
                        case "display":
                            this.Display = fetchDisplayDetails(N);
                            break;
                        case "sound":
                            this.Sound = fetchSoundDetails(N);
                            break;
                        case "memory":
                            this.Memory = fetchMemoryDetails(N);
                            break;
                        case "data":
                            this.Data = fetchDataDetails(N);
                            break;
                        case "camera":
                            this.Camera = fetchCameraDetails(N);
                            break;
                        case "features":
                            this.Feature = fetchFeatureDetails(N);
                            break;
                        case "battery": this.Battery = fetchBatteryDetails(N);
                            break;
                    }
                }
                Mobile.insertIntoDatabase(this);
            }
            catch (Exception)
            {
                //Do nothing.
            }

        }
        public Mobile(DataRow R)
        {
            this.Name = R["name"].ToString();
            this.Id = R["uniqueid"].ToString();
            this.Manufacturer = R["manufacturer"].ToString();
            this.ImageURL = R["image"].ToString();
            instantiateAllObject();
            this.Feature.Browser = R["browser"].ToString();
            this.Feature.Colors = R["colors"].ToString();
            this.Feature.CPU = R["cpu"].ToString();
            this.Feature.GPS = R["gps"].ToString();
            this.Feature.GPU = R["gpu"].ToString();
            this.Feature.Java = R["java"].ToString();
            this.Feature.Messaging = R["messaging"].ToString();
            this.Feature.OS = R["os"].ToString();
            this.Feature.Radio = R["radio"].ToString();
            this.Feature.Sensors = R["sensors"].ToString();
            this.General.Network2G = R["network2g"].ToString();
            this.General.Network3G = R["network3g"].ToString();
            this.General.Sim = R["sim"].ToString();
            this.General.Status = R["status"].ToString();
            this.Data.Bluetooth = R["bluetooth"].ToString();
            this.Data.EDGE = R["edge"].ToString();
            this.Data.GPRS = R["gprs"].ToString();
            this.Data.Speed = R["speed"].ToString();
            this.Data.USB = R["usb"].ToString();
            this.Data.WAN = R["wan"].ToString();
            this.Body.Dimensions = R["dimension"].ToString();
            this.Body.Weight = R["weight"].ToString();
            this.Battery.MusicPlay = R["musicplay"].ToString();
            this.Battery.StandBy = R["standby"].ToString();
            this.Battery.Talktime = R["talktime"].ToString();
            this.Battery.Type = R["batterytype"].ToString();
            this.Camera.Features = R["camerafeatures"].ToString();
            this.Camera.Primary = R["primary"].ToString();
            this.Camera.Secondary = R["secondary"].ToString();
            this.Camera.Video = R["video"].ToString();
            this.Display.Multitouch = R["mutitouch"].ToString();
            this.Display.Protection = R["protection"].ToString();
            this.Display.Size = R["size"].ToString();
            this.Display.Type = R["type"].ToString();
            this.Memory.CardSlot = R["cardslot"].ToString();
            this.Memory.InternalMemory = R["internalmemory"].ToString();
            this.Sound.AlertTypes = R["alerttypes"].ToString();
            this.Sound.Loudspeaker = R["loudspeaker"].ToString();
            this.UserRating.Design = R["design"].ToString();
            this.UserRating.Features = R["features"].ToString();
            this.UserRating.Performance = R["performance"].ToString();
            this.Status = true;
        }

        private BatteryDetails fetchBatteryDetails(HtmlNode N)
        {

            BatteryDetails O = new BatteryDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {

                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "&nbsp;": O.Type = nfoText;
                        break;
                    case "stand-by": O.StandBy = nfoText;
                        break;
                    case "talk time": O.Talktime = nfoText;
                        break;
                    case "music play": O.MusicPlay = nfoText;
                        break;
                }
            }
            return O;
        }
        private BodyDetails fetchBodyDetails(HtmlNode N)
        {

            BodyDetails O = new BodyDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {

                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "dimensions": O.Dimensions = nfoText;
                        break;
                    case "weight": O.Weight = nfoText;
                        break;
                }
            }
            return O;

        }
        private DataDetails fetchDataDetails(HtmlNode N)
        {

            DataDetails O = new DataDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {

                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "gprs": O.GPRS = nfoText;
                        break;
                    case "edge": O.EDGE = nfoText;
                        break;
                    case "speed": O.Speed = nfoText;
                        break;
                    case "wlan": O.WAN = nfoText;
                        break;
                    case "bluetooth": O.Bluetooth = nfoText;
                        break;
                    case "usb": O.USB = nfoText;
                        break;
                }
            }
            return O;

        }
        private CameraDetails fetchCameraDetails(HtmlNode N)
        {

            CameraDetails O = new CameraDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {

                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "primary": O.Primary = nfoText;
                        break;
                    case "secondary": O.Secondary = nfoText;
                        break;
                    case "video": O.Video = nfoText;
                        break;
                    case "features": O.Features = nfoText;
                        break;
                }
            }
            return O;

        }
        private DisplayDetails fetchDisplayDetails(HtmlNode N)
        {
            DisplayDetails O = new DisplayDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {

                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "type": O.Type = nfoText;
                        break;
                    case "multitouch": O.Multitouch = nfoText;
                        break;
                    case "size": O.Size = nfoText;
                        break;
                    case "protection": O.Protection = nfoText;
                        break;
                }
            }
            return O;
        }
        private FeatureDetails fetchFeatureDetails(HtmlNode N)
        {
            FeatureDetails O = new FeatureDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {

                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "browser": O.Browser = nfoText;
                        break;
                    case "colors": O.Colors = nfoText;
                        break;
                    case "cpu": O.CPU = nfoText;
                        break;
                    case "gpu": O.GPU = nfoText;
                        break;
                    case "gps": O.GPS = nfoText;
                        break;
                    case "java": O.Java = nfoText;
                        break;
                    case "messaging": O.Messaging = nfoText;
                        break;
                    case "os": O.OS = nfoText;
                        break;
                    case "radio": O.Radio = nfoText;
                        break;
                    case "sensors": O.Sensors = nfoText;
                        break;
                    
                }
            }
            return O;
        }
        private GeneralDetails fetchGeneralDetails(HtmlNode N)
        {


            GeneralDetails O = new GeneralDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {
                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "2g network": O.Network2G = nfoText;
                        break;
                    case "3g network": O.Network3G = nfoText;
                        break;
                    case "sim": O.Sim = nfoText;
                        break;
                    case "status": O.Status = nfoText;
                        break;
                }
            }
            return O;
        }
        private MemoryDetails fetchMemoryDetails(HtmlNode N)
        {

            MemoryDetails O = new MemoryDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {
                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "card slot": O.CardSlot = nfoText;
                        break;
                    case "internal": O.InternalMemory = nfoText;
                        break;
                }
            }
            return O;
        }
        private SoundDetails fetchSoundDetails(HtmlNode N)
        {


            SoundDetails O = new SoundDetails();
            string ttlText = null;
            string nfoText = null;
            foreach (HtmlNode T in N.Descendants("tr"))
            {
                string XPath = T.XPath;
                try { ttlText = T.SelectSingleNode(XPath + "/td[1]/a").InnerText.ToLower(); }
                catch (NullReferenceException)
                {
                    ttlText = T.SelectSingleNode(XPath + "/td[1]").InnerText.ToLower();
                }
                nfoText = T.SelectSingleNode(XPath + "/td[2]").InnerText;

                switch (ttlText)
                {

                    case "alert types": O.AlertTypes = nfoText;
                        break;
                    case "loudspeaker": O.Loudspeaker = nfoText;
                        break;
                }
            }
            return O;
        }
        private RatingInfo fetchRatingInfo(HtmlNode N)
        {

            RatingInfo O = new RatingInfo();
            try
            {
                O.Design = N.SelectSingleNode("//*[@id=\"vote-grph-design\"]/tt").InnerText;
            }
            catch (NullReferenceException)
            {
                O.Design = "0.0";
            } try
            {
                O.Features = N.SelectSingleNode("//*[@id=\"vote-grph-features\"]/tt").InnerText;
            }
            catch (NullReferenceException)
            {
                O.Design = "0.0";
            }
            try
            {
                O.Performance = N.SelectSingleNode("//*[@id=\"vote-grph-performance\"]/tt").InnerText;
            }
            catch (NullReferenceException)
            {
                O.Design = "0.0";
            }
            return O;
        }

        private static bool insertIntoDatabase(Mobile M)
        {

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["LocalDatabase"].ToString());
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "usp_insertMobile";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Connection = con;
            #region Setting Params
            cmd.Parameters.Add(new SqlParameter(@"uniqueid", M.Id));
            cmd.Parameters.Add(new SqlParameter(@"name", M.Name));
            cmd.Parameters.Add(new SqlParameter(@"manufacturer", M.Manufacturer));
            cmd.Parameters.Add(new SqlParameter(@"image", M.ImageURL));
            cmd.Parameters.Add(new SqlParameter(@"b_type", M.Battery.Type));
            cmd.Parameters.Add(new SqlParameter(@"b_standby", M.Battery.StandBy));
            cmd.Parameters.Add(new SqlParameter(@"b_talktime", M.Battery.Talktime));
            cmd.Parameters.Add(new SqlParameter(@"b_musicplay", M.Battery.MusicPlay));
            cmd.Parameters.Add(new SqlParameter(@"bd_weight", M.Body.Weight));
            cmd.Parameters.Add(new SqlParameter(@"bd_dimension", M.Body.Dimensions));
            cmd.Parameters.Add(new SqlParameter(@"c_primary", M.Camera.Primary));
            cmd.Parameters.Add(new SqlParameter(@"c_secondary", M.Camera.Secondary));
            cmd.Parameters.Add(new SqlParameter(@"c_features", M.Camera.Features));
            cmd.Parameters.Add(new SqlParameter(@"c_video", M.Camera.Video));
            cmd.Parameters.Add(new SqlParameter(@"dt_usb", M.Data.USB));
            cmd.Parameters.Add(new SqlParameter(@"dt_bluetooth", M.Data.Bluetooth));
            cmd.Parameters.Add(new SqlParameter(@"dt_wan", M.Data.WAN));
            cmd.Parameters.Add(new SqlParameter(@"dt_speed", M.Data.Speed));
            cmd.Parameters.Add(new SqlParameter(@"dt_edge", M.Data.EDGE));
            cmd.Parameters.Add(new SqlParameter(@"dt_gprs", M.Data.GPRS));
            cmd.Parameters.Add(new SqlParameter(@"d_protection", M.Display.Protection));
            cmd.Parameters.Add(new SqlParameter(@"d_multitouch", M.Display.Multitouch));
            cmd.Parameters.Add(new SqlParameter(@"d_size", M.Display.Size));
            cmd.Parameters.Add(new SqlParameter(@"d_type", M.Display.Type));
            cmd.Parameters.Add(new SqlParameter(@"f_colors", M.Feature.Colors));
            cmd.Parameters.Add(new SqlParameter(@"f_browser", M.Feature.Browser));
            cmd.Parameters.Add(new SqlParameter(@"f_cpu", M.Feature.CPU));
            cmd.Parameters.Add(new SqlParameter(@"f_gps", M.Feature.GPS));
            cmd.Parameters.Add(new SqlParameter(@"f_gpu", M.Feature.GPU));
            cmd.Parameters.Add(new SqlParameter(@"f_java", M.Feature.Java));
            cmd.Parameters.Add(new SqlParameter(@"f_messaging", M.Feature.Messaging));
            cmd.Parameters.Add(new SqlParameter(@"f_os", M.Feature.OS));
            cmd.Parameters.Add(new SqlParameter(@"f_radio", M.Feature.Radio));
            cmd.Parameters.Add(new SqlParameter(@"f_sensors", M.Feature.Sensors));
            cmd.Parameters.Add(new SqlParameter(@"f_extra", null));
            cmd.Parameters.Add(new SqlParameter(@"g_network2g", M.General.Network2G));
            cmd.Parameters.Add(new SqlParameter(@"g_network3g", M.General.Network3G));
            cmd.Parameters.Add(new SqlParameter(@"g_sim", M.General.Sim));
            cmd.Parameters.Add(new SqlParameter(@"g_status", M.General.Status));
            cmd.Parameters.Add(new SqlParameter(@"m_cardslot", M.Memory.CardSlot));
            cmd.Parameters.Add(new SqlParameter(@"m_internalmemory", M.Memory.InternalMemory));
            cmd.Parameters.Add(new SqlParameter(@"s_loudspeaker", M.Sound.Loudspeaker));
            cmd.Parameters.Add(new SqlParameter(@"s_alerttypes", M.Sound.AlertTypes));
            cmd.Parameters.Add(new SqlParameter(@"r_perfomance", M.UserRating.Performance));
            cmd.Parameters.Add(new SqlParameter(@"r_features", M.UserRating.Features));
            cmd.Parameters.Add(new SqlParameter(@"r_design", M.UserRating.Design));

            #endregion
            con.Open();
            int I = cmd.ExecuteNonQuery();
            con.Close();
            M.Status = true;
            return (I > 0) ? true : false;

        }
        public static Stream getResponseStream(string URL)
        {

            HttpWebRequest request = HttpWebRequest.Create(Mobile.RAWURL + URL) as HttpWebRequest;
            request.Timeout = 100000;
            request.ReadWriteTimeout = 100000;

            request.Accept = "*/*";
            request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729)";
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            WebResponse rep = request.GetResponse();
            return rep.GetResponseStream();
        }
    }
}