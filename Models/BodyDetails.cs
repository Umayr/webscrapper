﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class BodyDetails
    {
        string _weight;
        string _dimensions;

        public string Dimensions
        {
            get { return _dimensions; }
            set { _dimensions = value; }
        }

        public string Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
    }
}