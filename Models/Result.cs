﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class Result
    {
        List<Mobile> _data = new List<Mobile>();
        string _timeElapsed;
        int _count;

        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        public string TimeElapsed
        {
            get { return _timeElapsed; }
            set { _timeElapsed = value; }
        }
        public List<Mobile> Data
        {
            get { return _data; }
            set { _data = value; }
        }
    }
}