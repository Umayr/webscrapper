﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class MemoryDetails
    {

        public MemoryDetails()
        {
            InternalMemory = "N/A";
            CardSlot = "N/A";
        }
        string _internalMemory;
        string _cardSlot;

        public string InternalMemory
        {
            get { return _internalMemory; }
            set { _internalMemory = value; }
        }

        public string CardSlot
        {
            get { return _cardSlot; }
            set { _cardSlot = value; }
        }

    }
}