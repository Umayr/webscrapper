﻿using MobileApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MobileAPIv3.Models
{
    public class MobileResult:ActionResult
    {

        List<Mobile> _mobile = null;

        public List<Mobile> Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }
        public override void ExecuteResult(ControllerContext context)
        {
            HttpContextBase C = context.HttpContext;
            C.Response.Buffer = true;
            C.Response.ContentType = "application/json";

            C.Response.Write(JsonConvert.SerializeObject(Mobile, Formatting.None));
        }
    }
}