﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApi.Models
{
    public class BatteryDetails
    {
        string _musicPlay;
        string _talktime;
        string _standBy;
        string _type;

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string StandBy
        {
            get { return _standBy; }
            set { _standBy = value; }
        }

        public string Talktime
        {
            get { return _talktime; }
            set { _talktime = value; }
        }

        public string MusicPlay
        {
            get { return _musicPlay; }
            set { _musicPlay = value; }
        }

        public BatteryDetails()
        {
            this.Type = "N/A";
            this.Talktime = "N/A";
            this.StandBy = "N/A";
            this.MusicPlay = "N/A";
        }
    }
}