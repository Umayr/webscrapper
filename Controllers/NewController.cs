﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApi.Models;
using HtmlAgilityPack;
using System.Net;
using System.IO;

namespace MobileApi.Controllers
{
    public class NewController : Controller
    {
        //
        // GET: /New/

        private const string RAWURL = "http://www.gsmareana.com/";

        public CustomResult Index()
        {
            //Initiallizing Required Members
            TimeSpan reqStartTime = DateTime.Now.TimeOfDay;
            Result Result = new Result();
            string URL = "nokia-phones-1.php";
            List<Mobile> Mobiles = new List<Mobile>();


            HtmlDocument Document = new HtmlDocument();
            Document.Load(Mobile.getResponseStream(URL));

            HtmlNode html = Document.DocumentNode.Element("html");
            HtmlNodeCollection Lists = html.SelectNodes("//*[@class=\"makers\"]/ul");

            foreach (HtmlNode List in Lists)
            {
                HtmlNodeCollection ListChildren = List.ChildNodes;
                foreach (HtmlNode Child in ListChildren)
                {
                    if (Child.Name == "li")
                    {
                        //Mobiles.Add(new Mobile(Child));
                    }
                }

            }

            //Finalizing Result
            Result.Data = Mobiles;
            Result.Count = Mobiles.Count;
            //Result.UrlFetched = URL;
            TimeSpan reqCompletionTime = DateTime.Now.TimeOfDay - reqStartTime;
            Result.TimeElapsed = reqCompletionTime.Milliseconds + " ms";

            return new CustomResult() { R = Result };
            
        }


    }
}
