﻿using MobileApi.Models;
using MobileAPIv2.Models;
using MobileAPIv3.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace MobileAPIv3.Controllers
{
    public class ApiController : Controller
    {
        //
        // GET: /api/


        public CustomResult Index()
        {
            TimeSpan T = DateTime.Now.TimeOfDay;
            Result result = new Result();
            List<Mobile> MbList = getMobileList();
            result.Count = MbList.Count;
            result.Data = MbList;
            result.TimeElapsed = getElapsedTiming(T);
            return new CustomResult() { R = result };
        }

        public AutocompleteResult AutoComplete(string preText = null)
        {

            List<string> minList = new List<string>();
            List<Mobile> mList = getMobileList();

            if (preText != null)
            {

                mList = (from m in mList
                         where (m.Name.IndexOf(preText) >= 0)
                         select m).ToList<Mobile>();
            }

            foreach (Mobile M in mList)
            {
                minList.Add(M.Name);
            }

            return new AutocompleteResult() { R = minList };
        }

        public MobileResult Mobile(string name_like)
        {
            List<Mobile> MbList = getMobileList();
            MbList = (from M in MbList
                              where (M.Name.ToLower().IndexOf(name_like.ToLower()) >= 0)
                              select M).ToList<Mobile>();
            return new MobileResult() { Mobile = MbList };
        }
        public CustomResult Query(
            int? count,
            string os = null,
            string manufacturer = null,
            string has_camera = null,
            string has_gprs = null,
            string has_wifi = null,
            string has_edge = null,
            string has_bluetooth = null,
            string has_gpu = null,
            string has_gps = null,
            string has_radio = null
            )
        {
            #region Initializing

            TimeSpan T = DateTime.Now.TimeOfDay;
            Result result = new Result();
            List<Mobile> MbList = getMobileList();

            #endregion
            #region Name Check


            #endregion
            #region OS Check

            if (os != null)
            {
                MbList = (from M in MbList
                          where (M.Feature.OS.ToLower().IndexOf(os.ToLower()) >= 0)
                          select M).ToList();
            }
            #endregion
            #region Manufacturer Check

            if (manufacturer != null)
            {
                MbList = (from M in MbList
                          where (M.Manufacturer.ToLower() == manufacturer.ToLower())
                          select M).ToList();
            }
            #endregion
            #region Camera Check

            if (has_camera != null)
            {
                if (has_camera == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Camera.Primary.ToLower() == "n/a" || M.Camera.Primary.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Camera.Primary.ToLower() == "n/a" || M.Camera.Primary.ToLower() == "no")
                              select M).ToList();
                }
            }

            #endregion
            #region GPRS Check
            if (has_gprs != null)
            {
                if (has_gprs == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Data.GPRS.ToLower() == "n/a" || M.Data.GPRS.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Data.GPRS.ToLower() == "n/a" || M.Data.GPRS.ToLower() == "no")
                              select M).ToList();
                }
            }
            #endregion
            #region Wifi Check

            if (has_wifi != null)
            {
                if (has_wifi == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Data.WAN.ToLower() == "n/a" || M.Data.WAN.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Data.WAN.ToLower() == "n/a" || M.Data.WAN.ToLower() == "no")
                              select M).ToList();
                }
            }

            #endregion
            #region GPU Check

            if (has_gpu != null)
            {
                if (has_gpu == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Feature.GPU.ToLower() == "n/a" || M.Feature.GPU.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Feature.GPU.ToLower() == "n/a" || M.Feature.GPU.ToLower() == "no")
                              select M).ToList();
                }
            }

            #endregion
            #region EDGE Check
            if (has_edge != null)
            {
                if (has_edge == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Data.EDGE.ToLower() == "n/a" || M.Data.EDGE.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Data.EDGE.ToLower() == "n/a" || M.Data.EDGE.ToLower() == "no")
                              select M).ToList();
                }
            }

            #endregion
            #region Bluetooth Check
            if (has_bluetooth != null)
            {
                if (has_bluetooth == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Data.Bluetooth.ToLower() == "n/a" || M.Data.Bluetooth.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Data.Bluetooth.ToLower() == "n/a" || M.Data.Bluetooth.ToLower() == "no")
                              select M).ToList();
                }
            }
            #endregion
            #region GPS Check
            if (has_gps != null)
            {
                if (has_gps == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Feature.GPS.ToLower() == "n/a" || M.Feature.GPS.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Feature.GPS.ToLower() == "n/a" || M.Feature.GPS.ToLower() == "no")
                              select M).ToList();
                }
            }
            #endregion
            #region Radio Check
            if (has_radio != null)
            {
                if (has_radio == "true")
                {
                    MbList = (from M in MbList
                              where (!(M.Feature.Radio.ToLower() == "n/a" || M.Feature.Radio.ToLower() == "no"))
                              select M).ToList();
                }
                else
                {
                    MbList = (from M in MbList
                              where (M.Feature.Radio.ToLower() == "n/a" || M.Feature.Radio.ToLower() == "no")
                              select M).ToList();
                }
            }
            #endregion
            #region Count

            if (count.HasValue)
            {
                MbList = MbList.Take(count.Value).ToList<Mobile>();
            }
            #endregion
            #region Finalizing
            result.Count = MbList.Count;
            result.Data = MbList;
            result.TimeElapsed = getElapsedTiming(T);
            return new CustomResult() { R = result };
            #endregion
        }


        private string getElapsedTiming(TimeSpan T)
        {

            return (DateTime.Now.TimeOfDay - T).TotalMilliseconds + " ms";
        }
        private List<Mobile> getMobileList()
        {

            DataTable dt = Process.GetMobileFromDatabase();
            List<Mobile> MbList = new List<Mobile>();

            foreach (DataRow R in dt.Rows)
            {
                MbList.Add(new Mobile(R));
            }

            return MbList;

        }
    }
}
