﻿using HtmlAgilityPack;
using MobileApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MobileAPIv3.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public bool Insert(string Name, string URL)
        {

            Mobile M = new Mobile(Name, URL);

            return M.Status;
        }

        [HttpPost]
        public string StartScraping(string URL)
        {
            TimeSpan preRequestTime = DateTime.Now.TimeOfDay;

            List<string> URLs = new List<string>();
            List<string> Names = new List<string>();
            
            HtmlDocument Document = new HtmlDocument();
            Document.Load(Mobile.getResponseStream(URL.ToString()));

            HtmlNode html = Document.DocumentNode.Element("html");
            HtmlNodeCollection Lists = html.SelectNodes("//*[@class=\"makers\"]/ul");

            foreach (HtmlNode List in Lists)
            {
                HtmlNodeCollection ListChildren = List.ChildNodes;
                foreach (HtmlNode Child in ListChildren)
                {
                    if (Child.Name == "li")
                    {
                        Names.Add(Child.FirstChild.Element("strong").InnerText);
                        URLs.Add(Child.Element("a").Attributes["href"].Value);
                    }
                }

            }
            var obj = new
            {
                MobileLinks = URLs,
                MobileNames = Names,
                TimeElapsed = (DateTime.Now.TimeOfDay - preRequestTime).TotalMilliseconds + " ms"
            };
            
            return (new JavaScriptSerializer()).Serialize(obj);
        }

    }
}
